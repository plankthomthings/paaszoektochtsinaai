#!/bin/bash
echo '['
for img in `ls img/*.jpg`
do 
    base=${img##*/}
    id=${base%.*}
    echo "{ 
     id:'$id', img: '$img' , ext: '${img#*.}',
     title:'$id', 
     description: '$id',
     eieren: [
        { 
            id: 'ster', ext: 'svg', title: 'eitje', 
            verstopt : { width: '10px', top: '55px', left: '440px' }
        }
    ]
},"
done
echo ']'