const EI_STATUS = {
    VERSTOPT: 'verstopt',
    GEVONDEN: 'gevonden',
    IN_MANDJE: 'in_mandje',
    WORDT_GEPLAATST: 'geplaatst'
}
const PLAATS_STATUS = {
    ONBEKEND: 'onbekend',
    GEKEND: 'gekend',
    EITJES_GEVONDEN: 'eitjes',
    ALLES_GEVONDEN: 'alles'
}
const EENVOUD = {
    SUPERMOEILIJK: 'supermoeilijk',
    MOEILIJK: 'moeilijk',
    GEWOON: 'gewoon',
    GEMAKKELIJK: 'gemakkelijk',
    EENVOUDIG: 'eenvoudig',
}
const GEBEURT = {
    EI_VERANDERD: 'ei',
    PLAATS_VERANDERD: 'kaart',
    EENVOUD_GEWIJZIGD: 'eenvoud',
    KAART_GELADEN: 'kaart',
    BELONING_GELADEN: 'beloning',
    EI_VERPLAATST: 'edit_verplaatst',
    EI_SPEC_EDIT: 'edit_spec'
}
const DEFAULT = {
    EI: {
        id: 'ster',
        title: 'een eitje',
        status: EI_STATUS.VERSTOPT
    },
    EI_LIJST: [
        { id: 'ster', verstopt: { right: '50px' } },
        { id: 'rood', verstopt: { right: '150px' } },
        { id: 'appelblauwzeegroen', verstopt: { right: '250px' } },
        { id: 'bloem', verstopt: { right: '350px' } },
        { id: 'hart', verstopt: { right: '450px' } },
        { id: 'oranje', verstopt: { right: '550px' } }
    ],
    EI_STATUS: {
        VERSTOPT: {
            opacity: 0.99,
            width: '20px',
            bottom: '155px', right: '250px',
            transform: 'rotateZ(0deg)'
        },
        GEPLAATST: {
            opacity: 1
        },
        GEVONDEN: {
            opacity: 1, width: '50px',
            transform: 'rotateZ(360deg)'
        },
        IN_MANDJE: {
            width: '30px',
            transform: 'rotateZ(0deg)',
            bottom: '70px',
            right: '85px'
        }
    },
    PLAATS: {
        status: PLAATS_STATUS.ONBEKEND,
        width: '600px',
        eenvoud: EENVOUD.MOEILIJK,
        mandje: {
            width: '150px',
            bottom: '30px',
            right: '20px',
        },
        score: 5,
        title: "",
        description: ""
    },
    KAART: {
        max_plaatsen: 99,
        max_eitjes: 3
    },
    BELONING: { width: 800 }
}

function asStyle(attrs) {
    return Object.keys(attrs).map(k => `${k}:${attrs[k]}`).join('; ')
}
function updateInnerHtml(id, html) {
    if (id && html) {
        document.getElementById(id).innerHTML = html
    }
}
function sorry() {
    updateInnerHtml('sorry', `
Helaas, deze
    <a href="https://developer.mozilla.org/en-US/docs/Web/API/Element/animate#Browser_compatibility">
            browser kan je niet helpen
    </a>
om eitjes te zoeken.<br> 
Gebruik Chrome, Firefox, Safari (enkel mac), of Edge (versie 79 of hoger)`
    )
}
class Ei {
    constructor(plaats, props) {
        Object.defineProperty(this, 'plaats', { value: plaats })
        Object.assign(this, DEFAULT.EI, props)
        this.initAttrs()
    }
    get fullId() {
        return `${this.plaats.id}_${this.id}`
    }
    get urlPath() { return this.plaats.urlPath }
    get defaultImg() { return `img/ei/${this.id}.${this.ext || 'svg'}` }
    get imageUrl() {
        return this.urlPath + (this.img || this.defaultImg)
    }
    initAttrs() {
        this.verstopt = Object.assign({}, DEFAULT.EI_STATUS.VERSTOPT, this.verstopt)
        this.gevonden = Object.assign({}, this.verstopt, DEFAULT.EI_STATUS.GEVONDEN, this.gevonden)
        this.in_mandje = Object.assign({}, this.gevonden, DEFAULT.EI_STATUS.IN_MANDJE, this.in_mandje)
        this.geplaatst = Object.assign({}, this.verstopt, DEFAULT.EI_STATUS.GEPLAATST, this.geplaatst)
        this.in_mandje.right = `${85 + (Math.random() * 100) - 50}px`
        this.in_mandje.transform = `rotateZ(${Math.random() * 30}deg)`
        this.in_mandje.bottom = `${60 + Math.random() * 20}px`
    }
    styleAttrs(status, eenvoud) {
        status = status || this.status || EI_STATUS.VERSTOPT
        eenvoud = eenvoud || this.plaats.eenvoud || EENVOUD.MOEILIJK
        const attrs = this[status] || {}
        if (status !== EI_STATUS.VERSTOPT) { return attrs }
        switch (eenvoud) {
            case EENVOUD.SUPERMOEILIJK:
            case EENVOUD.MOEILIJK:
                return Object.assign({}, attrs, { opacity: attrs.opacity / 2 })
            case EENVOUD.GEWOON:
                return attrs
            case EENVOUD.GEMAKKELIJK:
                return Object.assign({}, attrs, { opacity: (1 + attrs.opacity) / 2 })
            case EENVOUD.EENVOUDIG:
                return Object.assign({}, attrs, { opacity: 1 })
            default:
                return {}
        }
    }
    get currentStyleAttrs() { return this.styleAttrs() }
    get beeldHtml() {
        const ref = this.fullId
        return `
<div 
    id="${ref}" 
    class="ei ${this.status}" 
    onmouseover="hoverEi('${ref}')"
    onmouseleave="leaveEi('${ref}')"
    onclick="clickEi('${ref}')"
    style="${asStyle(this.currentStyleAttrs)}"
>
    <img src="${this.imageUrl}">
</div>`
    }
    get editorHtml() {
        const ref = this.fullId
        return `
<div 
    id="${ref}" 
    class="ei gevonden"  
    style="${asStyle(this.currentStyleAttrs)}"
    onmouseover="hoverEi('${ref}')"
    onmouseleave="leaveEi('${ref}')"
    draggable="true" 
    ondragstart="dragStartEi(event, '${ref}')" 
    ondragend="dragEndEi(event, '${ref}')"
>
    <img src="${this.imageUrl}"  >
</div>`
    }
    get element() { return document.getElementById(this.fullId) }
    hover() {
        if (this.status === EI_STATUS.VERSTOPT) {
            const current = this.currentStyleAttrs
            switch (this.plaats.eenvoud) {
                case EENVOUD.SUPERMOEILIJK:
                case EENVOUD.EENVOUDIG:
                    return
                case EENVOUD.MOEILIJK:
                    var opacity = current.opacity + 0.05
                    break
                default:
                    opacity = Math.min((1 + current.opacity) / 2)
            }
            this.animateFromTo(current, Object.assign({}, current, { opacity: opacity }))
        }
    }
    mouseleave() {
        if (this.status === EI_STATUS.VERSTOPT) {
            this.animateTo(this.currentStyleAttrs, 500)
        }
    }
    click() {
        const p = this.plaats
        const k = p.kaart
        const renderKaart = () => k.render(GEBEURT.EI_VERANDERD)
        switch (this.status) {
            case EI_STATUS.VERSTOPT:
                const currentStyleAttrs = this.currentStyleAttrs
                this.status = EI_STATUS.GEVONDEN
                var onfinish = renderKaart
                if (p.eitjesGevonden) {
                    onfinish = () => {
                        p.eieren.forEach(ei => ei.click())
                        setTimeout(() => p.toonBeloning(), 2000)
                    }
                }
                this.animateFromTo(currentStyleAttrs, this.styleAttrs(EI_STATUS.GEVONDEN), 1000).onfinish = onfinish
                break
            case EI_STATUS.GEVONDEN:
                this.animateTo(this.styleAttrs(EI_STATUS.IN_MANDJE), 1500, 'ease-in-out').onfinish = renderKaart
                this.status = EI_STATUS.IN_MANDJE
                break
            case EI_STATUS.IN_MANDJE:
                this.spin().onfinish = renderKaart
                break
            case EI_STATUS.WORDT_GEPLAATST:
                break
        }
    }
    animateTo(attrs, time, easing = undefined) {
        return this.animateFromTo(this.currentStyleAttrs, attrs, time, easing)
    }
    animateFromTo(fromAttrs, toAttrs, time, easing = undefined) {
        return this.element.animate(
            [fromAttrs, toAttrs],
            { duration: time, fill: 'forwards', easing: easing }
        )
    }
    spin() {
        return this.element.animate(
            [
                { transform: this.in_mandje.transform + ' rotateY(0deg)' },
                { transform: this.in_mandje.transform + ' rotateY(360deg)' }
            ],
            { duration: 500, fill: 'forwards', iterations: 3 }
        )
    }
    opgenomen(event) {
        console.log('opgenomen', event, this)
        this.geplaatst.opgenomen = {
            clientX: event.clientX, clientY: event.clientY,
            offsetX: event.offsetX, offsetY: event.offsetY,
            height: event.srcElement.height, width: event.srcElement.width
        }
        console.log('opgenomen_xy', this.geplaatst.opgenomen)
    }
    neergezet(event) {
        console.log('neergezet', event, this)
        const opgenomen = this.geplaatst.opgenomen
        const deltaX = event.clientX - opgenomen.clientX - opgenomen.offsetX
        const deltaY = event.clientY - opgenomen.clientY + opgenomen.height - opgenomen.offsetY
        // const deltaX = event.clientX - opgenomen.clientX 
        // const deltaY = event.clientY - opgenomen.clientY

        console.log('delta_xy', { x: deltaX, y: deltaY })
        changeXY(this.geplaatst, deltaX, deltaY)
        changeXY(this.verstopt, deltaX, deltaY)
        changeXY(this.gevonden, deltaX, deltaY)
        delete this.geplaatst.opgenomen
        this.animateTo(this.styleAttrs(), 0)
        updateInnerHtml('editor', `
        <textarea id="eispecs" >${this.plaats.eitjesEditorJson}</textarea>
        <button onclick="updateEiSpecs()">Verander</button>
        `)
        bewaar(this.plaats.kaart)
    }
}

class Plaats {
    constructor(kaart, props) {
        Object.defineProperty(this, 'kaart', { value: kaart })
        Object.assign(this, DEFAULT.PLAATS, props)
        this.title = this.title || this.id
        this.img = this.img || `img/plaats/${this.id}.${this.ext || 'jpg'}`
        this.initEieren(props.eieren)
    }
    initEieren(eieren) {
        if (!eieren) {
            eieren = DEFAULT.EI_LIJST
            if (editorMode) {
                eieren = eieren.map(
                    ei => Object.assign({ status: EI_STATUS.WORDT_GEPLAATST }, ei)
                )
            }
        }
        eieren = (eieren || DEFAULT.EI_LIJST).map(eiProps => new Ei(this, eiProps))
        if (!editorMode) {
            const max_eitjes = Number(this.max_eitjes || this.kaart.max_eitjes)
            while (eieren.length > max_eitjes) {
                eieren.splice(Math.floor(Math.random() * eieren.length), 1)
            }
        }
        this.eieren = eieren
    }
    activeer() {
        if (this.status == PLAATS_STATUS.ONBEKEND) {
            this.status = PLAATS_STATUS.GEKEND
        }
    }
    toonBeloning() {
        this.status = PLAATS_STATUS.EITJES_GEVONDEN
        this.kaart.toonBeloning(this.beloning)
    }
    get urlPath() { return this.kaart.urlPath }

    eiById(id) {
        return this.eieren.find(ei => ei.fullId === id || ei.id === id)
    }
    beeldHtml(event) {
        var onload, eitjesClasses
        switch (event) {
            case GEBEURT.EENVOUD_GEWIJZIGD:
            case GEBEURT.BELONING_GELADEN:
                // geen state veranderd, opacity rendering al gebeurd in animatie
                return
            case GEBEURT.EI_VERANDERD:
                // eitjes in beeld, dus geen animatie die ons kan verraden
                onload = ''
                eitjesClasses = 'eitjes'
                break
            case GEBEURT.KAART_GELADEN:
            case GEBEURT.PLAATS_VERANDERD:
            default:
                // toon eitjes (langzaam) nadat foto is ingeladen
                onload = 'onload="toonNieuweEitjes()"'
                eitjesClasses = 'eitjes nieuw'
        }
        return `
<div class="plaats" id="${this.id}">
    <span class="msg ${this.eitjesGevonden ? '' : 'verberg'}">Hier heb je alle Eitjes Gevonden!</span>
    <img class="foto ${this.classes}" ${onload} width="${this.width}" src="${this.urlPath}${this.img}" alt="Even geduld, het beeld voor '${this.title}' komt er aan ...">
    <img class='mandje' src="${this.urlPath}img/mandje.png" style="${asStyle(this.mandje)}">
    <div class="${eitjesClasses}">${this.eitjesBeeldHtml}</div>
    <img class='mandje' src="${this.urlPath}img/mandje_voorkant.png" style="${asStyle(this.mandje)}">
    <label class='mandje-label' style="${asStyle(this.mandje)}">${this.gevondenEitjes} / ${this.aantalEitjes}</label>
</div> `
    }
    get eitjesBeeldHtml() { return this.eieren.map(ei => ei.beeldHtml).join(' ') }
    beeldEditorHtml(event) {
        return `
        <div class="plaats" id="${this.id}">
            <img class="foto ${this.classes}" onload="toonNieuweEitjes()" width="${this.width}" src="${this.urlPath}${this.img}">
            <img class='mandje' src="${this.urlPath}img/mandje.png" style="${asStyle(this.mandje)}">
            <div class="eitjes">${this.eitjesEditorHtml}</div>
            <img class='mandje' src="${this.urlPath}img/mandje_voorkant.png" style="${asStyle(this.mandje)}">
            <label class='mandje-label' style="${asStyle(this.mandje)}">${this.gevondenEitjes} / ${this.aantalEitjes}</label>
        </div>
        `
    }
    get eitjesEditorJson() {
        return JSON.stringify(
            this.eieren.map(ei => ({
                id: ei.id,
                status: ei.status,
                img: (ei.img || ei.defaultImg),
                verstopt: {
                    opacity: ei.verstopt.opacity,
                    width: ei.verstopt.width,
                    bottom: ei.verstopt.bottom,
                    right: ei.verstopt.right,
                    transform: ei.verstopt.transform || 'rotateZ(0deg)'
                }
            })),
            null,
            4
        )
    }
    get eitjesEditorHtml() { return this.eieren.map(ei => ei.editorHtml).join(' ') }
    get gevondenEitjes() { return this.eieren.filter(_ => _.status !== EI_STATUS.VERSTOPT && _.status !== EI_STATUS.WORDT_GEPLAATST).length }
    get aantalEitjes() { return this.eieren.length }
    get eitjesGevonden() { return this.aantalEitjes === this.gevondenEitjes }
    eenvoudOptieHtml(optie) {
        return `
        <div 
            class="${this.eenvoud === optie ? 'selected' : ''}"
            onclick="kaart.zetEenvoud('${this.id}', '${optie}')"
        >${optie}</div>
        `
    }
    get beschrijvingHtml() {
        return `
        <h2>${this.title}</h2>
        <span class="description">${this.description}</span>
        <div>Je kan hier ${this.aantalEitjes} eitjes vinden.</div>
        <div class="menu">${Object.values(EENVOUD).map(_ => this.eenvoudOptieHtml(_)).join(' | ')}</div>
        `
    }
    linkHtml(active) {
        const overgebleven = this.aantalEitjes - this.gevondenEitjes
        var classes = "link"
        if (this.eitjesGevonden) classes += " alles"
        if (active) classes += " active"
        return `
            <div class="${classes}" onclick="kaart.zoekInPlaats('${this.id}')">
                ${this.title}
                <span class="status">(nog ${overgebleven} eitje${overgebleven === 1 ? '' : 's'} ...)</span>
                <span class="gedaan">Proficiat: Alle eitjes gevonden!</span>
            </div>`
    }
    render(event) {
        updateInnerHtml('beschrijving', this.beschrijvingHtml)
        if (editorMode) {
            updateInnerHtml('beeld', this.beeldEditorHtml(event))
        } else {
            updateInnerHtml('beeld', this.beeldHtml(event))
        }
    }
    toonNieuweEitjes() {
        const eitjes = document.querySelector('#beeld .eitjes')
        if (eitjes.animate) {
            eitjes.animate([{ opacity: 0 }, { opacity: 1 }], { duration: 5000, fill: 'forwards' })
        } else {
            sorry()
        }
    }
    zetEenvoud(eenvoud) {
        this.eenvoud = eenvoud
        this.eieren.forEach(ei => ei.animateTo(ei.styleAttrs(null, eenvoud), 200))
        this.render(GEBEURT.EENVOUD_GEWIJZIGD)
    }
}

class Beloning {
    constructor(kaart, props) {
        Object.defineProperty(this, 'kaart', { value: kaart })
        Object.assign(this, DEFAULT.BELONING, props)
        this.img = this.img || `img/beloning/${this.id}.${this.ext || 'jpg'}`
    }
    get urlPath() { return this.kaart.urlPath }

    get beeldHtml() {
        return `
            <div class="beloning" id="${this.id}">
                <span class="msg">Beloning!</span>
                <img class="foto" width="${this.width}" onclick="kaart.zoekVolgendePlaats('${this.id}')" src="${this.kaart.urlPath}${this.img}">
            </div>
        `
    }
    get beschrijvingHtml() {
        return `
            <h2>${this.title}</h2>
            <span class="description">${this.description}</span>
        `
    }
    activeer() {
        if (this.nieuwe_plaatsen) {
            this.nieuwe_plaatsen.map(p => {
                this.kaart.plaatsById(p).activeer()
            })
        }
    }
    render(event) {
        updateInnerHtml('beschrijving', this.beschrijvingHtml)
        updateInnerHtml('beeld', this.beeldHtml)
    }
}
class Kaart {
    constructor(props, onChange) {
        onChange = onChange || (_ => _)
        Object.defineProperty(this, 'onChange', { value: onChange })
        Object.assign(this, DEFAULT.KAART, props)
        props.plaatsen.sort((p1, p2) => p2.score - p1.score)

        this.plaatsen = props.plaatsen.map(plProps => new Plaats(this, plProps))
        if (!editorMode) {
            this.plaatsen.splice(this.max_plaatsen)
        }
        this.beloningen = props.beloningen
            .map(belProps => new Beloning(this, belProps))

        if (!this.huidigeBeloningId && !this.huidigePlaatsId) {
            this.huidigePlaatsId = this.gekendePlaatsen[0].id
        }
    }
    plaatsById(id) {
        const plaats = this.plaatsen.find(p => p.id === id)
        if (!plaats) {
            console.log(`plaats '${id}' niet gevonden`)
        }
        return plaats
    }
    beloningById(id) {
        const beloning = this.beloningen.find(b => b.id == id)
        if (!beloning) {
            console.log(`beloning '${id}' niet gevonden`)
        }
        return beloning
    }
    toonBeloning(id) {
        this.huidigePlaatsId = null
        this.huidigeBeloningId = id
        if (id) {
            this.huidigeBeloning.activeer()
        }
        this.render(GEBEURT.BELONING_GELADEN)
    }
    get huidigeBeloning() {
        return this.beloningById(this.huidigeBeloningId)
    }
    get huidigePlaats() {
        return this.plaatsById(this.huidigePlaatsId)
    }
    get allesGevonden() {
        return !this.plaatsen.find(
            p => !(
                p.status == PLAATS_STATUS.EITJES_GEVONDEN || p.status == PLAATS_STATUS.ONBEKEND
            )
        )
    }
    get gekendePlaatsen() {
        if (editorMode) return this.plaatsen
        return this.plaatsen.filter(
            plaats => plaats.status != PLAATS_STATUS.ONBEKEND
        )
    }
    get lijstHtml() {
        const huidigePlaatsId = this.huidigePlaatsId
        var html = this.gekendePlaatsen
            .map(plaats => plaats.linkHtml(plaats.id === huidigePlaatsId))
            .join(' ')
        return html
    }
    get titelHtml() {
        return `
        <h1>${this.title}</h1>
        <div class="menu opnieuw" ><div onclick="opnieuw()">opnieuw</div></div>
        <div class="description">${this.description}</div>
        `
    }
    render(event) {
        updateInnerHtml('lijst', this.lijstHtml)
        updateInnerHtml('titel', this.titelHtml)
        if (this.allesGevonden) {
            this.huidigePlaatsId = null
            this.huidigeBeloningId = this.eind_beloning
        }
        if (this.huidigePlaatsId) {
            this.huidigePlaats.render(event)
        } else if (this.huidigeBeloningId) {
            this.huidigeBeloning.render(event)
        }
        this.onChange(this)
    }

    zoekInPlaats(plaatsId) {
        if (!plaatsId) {
            const plaats = this.gekendePlaatsen.find(
                p => p.status != PLAATS_STATUS.EITJES_GEVONDEN
            )
            plaatsId = plaats && plaats.id
        }
        this.huidigePlaatsId = plaatsId
        this.huidigeBeloningId = null
        if (plaatsId) this.render(GEBEURT.PLAATS_VERANDERD)
    }

    zoekVolgendePlaats(beloningId) {
        const beloningPlaats = this.beloningById(beloningId).nieuwe_plaatsen.find(
            p => p.status != PLAATS_STATUS.EITJES_GEVONDEN
        )
        this.zoekInPlaats(beloningPlaats && beloningPlaats.id)
    }

    zetEenvoud(plaatsId, eenvoud) {
        this.plaatsById(plaatsId).zetEenvoud(eenvoud)
    }
}

var editorMode, localStorageId, kaartOverrides
function start(zoektochtLoc, { editor, reset, max_eitjes, max_plaatsen }) {
    editorMode = editor && (editor.toUpperCase() === 'TRUE')
    localStorageId = 'zoektocht$' + zoektochtLoc
    kaartOverrides = {}
    if (max_eitjes) kaartOverrides.max_eitjes = max_eitjes
    if (max_plaatsen) kaartOverrides.max_plaatsen = max_plaatsen
    if (reset && (reset.toUpperCase() === 'TRUE')) {
        window.localStorage.removeItem(localStorageId)
    }
    var urlPath = zoektochtLoc + '/'
    var zoektochtData = window.localStorage.getItem(localStorageId)
    if (zoektochtData) {
        kaart = new Kaart(JSON.parse(zoektochtData), bewaar)
        kaart.render(GEBEURT.KAART_GELADEN)
    } else {
        laadKaart(urlPath)
    }
}

var kaart
function laadKaart(urlPath) {
    const kaartUrl = `${urlPath}kaart.json`
    const request = new XMLHttpRequest()
    request.open('GET', kaartUrl)
    request.responseType = 'json'
    request.onload = resp => {
        zoektochtData = request.response
        zoektochtData.urlPath = urlPath
        Object.assign(zoektochtData, kaartOverrides)
        kaart = new Kaart(zoektochtData, bewaar)
        kaart.render(GEBEURT.KAART_GELADEN)
    }
    request.send()
}

function opnieuw() {
    window.localStorage.removeItem(localStorageId)
    laadKaart(kaart.urlPath)
}

function toonNieuweEitjes() {
    kaart.huidigePlaatsId && kaart.huidigePlaats.toonNieuweEitjes()
}

function hoverEi(id) {
    kaart.huidigePlaatsId && kaart.huidigePlaats.eiById(id).hover()
}

function leaveEi(id) {
    kaart.huidigePlaatsId && kaart.huidigePlaats.eiById(id).mouseleave()
}

function clickEi(id) {
    kaart.huidigePlaatsId && kaart.huidigePlaats.eiById(id).click()
}

function dragStartEi(event, id) {
    kaart.huidigePlaats.eiById(id).opgenomen(event)
}
function dragEndEi(event, id) {
    event.preventDefault();
    kaart.huidigePlaats.eiById(id).neergezet(event)
}
function updateEiSpecs(event) {
    const eieren = JSON.parse(document.getElementById('eispecs').value)
    kaart.huidigePlaats.initEieren(eieren)
    kaart.render(GEBEURT.EI_SPEC_EDIT)
}
document.addEventListener("dragover", event => event.preventDefault(), false)
document.addEventListener("drop", event => event.preventDefault(), false)


function bewaar(kaart) {
    const data = JSON.stringify(kaart)
    window.localStorage.setItem(localStorageId, data)
    return kaart
}

function changeXY(styleAttrs, deltaX, deltaY) {
    const update = {}
    if (styleAttrs.bottom) {
        const bottom = Number(styleAttrs.bottom.replace('px', ''))
        update.bottom = `${bottom - deltaY}px`
    }
    if (styleAttrs.top) {
        const top = Number(styleAttrs.top.replace('px', ''))
        update.top = `${top + deltaY}px`
    }
    if (styleAttrs.right) {
        const right = Number(styleAttrs.right.replace('px', ''))
        update.right = `${right - deltaX}px`
    }
    if (styleAttrs.left) {
        const left = Number(styleAttrs.left.replace('px', ''))
        update.left = `${left + deltaX}px`
    }
    Object.assign(styleAttrs, update)
}